#!/bin/bash

dest_spec_file_path="/Users/aron/MyWorkSpaces/CodeRepos/babybus-gt-spec/FirebaseABTesting/3.2.0/FirebaseABTesting.podspec"
# 修改配置
oriurl="https://github.com/firebase/firebase-ios-sdk.git"
replacedurl="http://git.babybus.co/thirdadsdk/firebase-ios-sdk.git"
# oriurl="Google"
# replacedurl="Google---------"
sed -i '' '{
	s#'"${oriurl}"'#'"${replacedurl}"'#g
}' ${dest_spec_file_path}
#!/bin/bash

specs_path="/Users/aron/MyWorkSpaces/CodeRepos/babybus-gt-spec"
firebase_repo_path="/Users/aron/MyWorkSpaces/CodeRepos/firebase-ios-sdk"

cd ${firebase_repo_path}
for item in $(ls *.podspec ); do
	echo $item

	# versionLine=$(grep "s.version *= *" ${item})
	# echo "versionLine = ${versionLine}"

	version=$(cat ${item} | awk -F= '/s\.version[ ]+/ {print $2}' | sed 's/ //g' | sed s/\'//g)
	echo "version = ${version}"

	name=$(cat ${item} | awk -F= '/s\.name[ ]+/ {print $2}' | sed 's/ //g' | sed s/\'//g)
	echo "name = ${name}"

	repo_spec_path="${specs_path}/${name}"
	if [[ ! -d ${repo_spec_path} ]]; then
		echo "索引库目录不存在，创建 ${repo_spec_path}"
		mkdir ${repo_spec_path}
	fi

	repo_spec_version_path="${repo_spec_path}/${version}"
	if [[ ! -d ${repo_spec_version_path} ]]; then
		echo "索引库版本目录不存在，创建 ${repo_spec_version_path}"
		mkdir ${repo_spec_version_path}
	fi

	# 拷贝Podspec文件
	dest_spec_file_path="${repo_spec_version_path}/${item}"
	cp ${item} ${dest_spec_file_path}

	# 修改配置
	oriurl="https://github.com/firebase/firebase-ios-sdk.git"
	replacedurl="http://git.babybus.co/thirdadsdk/firebase-ios-sdk.git"
	sed -i '' '{
		s#'"${oriurl}"'#'"${replacedurl}"'#g
	}' ${dest_spec_file_path}

	echo ""

done

# 推送到索引库
cd ${specs_path}
git add .
git commit -m "update firebase"
git push
ReadMe.md


```
# 加载&开始任务
launchctl load -w com.zyt.autocommit.plist
launchctl start -w com.zyt.autocommit.plist
# 查看任务
launchctl list | grep 'com.zyt.autocommit'

# 停止&卸载任务
launchctl stop com.zyt.autocommit.plist
launchctl unload -w com.zyt.autocommit.plist
```
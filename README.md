# ShellSnippet

#### 介绍
一些有用的Shell脚本  

介绍文章：[实用脚本分享](https://my.oschina.net/FEEDFACF/blog/3045369)  

[检测图片资源包的脚本checkImages.sh](https://gitee.com/dhar/ShellSnippet/blob/master/checkImages.sh)  
[批量转换文件编码的脚本convertEncode.sh](https://gitee.com/dhar/ShellSnippet/blob/master/convertEncode.sh)  
[生成随机字符串的脚本randomchargen.sh](https://gitee.com/dhar/ShellSnippet/blob/master/randomchargen.sh)  

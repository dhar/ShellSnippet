#/bin/sh

imagesFolder="/Users/aron/Desktop/CheckImages/IOS"
outpuLogFile="$(pwd)/outLogFile.txt"
echo "" > ${outpuLogFile}

# 输入文件夹
echo -n "请输入图片目录: "
read imagesFolder
if [[ -d $imagesFolder ]]; then
	echo "图片目录正确"
else
	echo -n "输入的目录无效，请重新执行脚本"
fi

# 获取下面的所有文件
lastHandleFile=""
lastHandleImageWidthHeight=""
for imageFullName in $(ls ${imagesFolder}); do
	echo "imageFullName = ${imageFullName} lastHandleFile = ${lastHandleFile}"
	pureImageName=`echo ${imageFullName} | sed 's/@.*.png//g' | sed 's/\.png//g'`
	if [[ ${pureImageName} == ${lastHandleFile} ]]; then
		# 判断大小
		imagePath="${imagesFolder}/${imageFullName}"
		ImageWidth=`sips -g pixelWidth ${imagePath} | awk -F: '{print $2}'`
		ImageHeight=`sips -g pixelHeight ${imagePath} | awk -F: '{print $2}'`
		height=`echo $ImageHeight`
		width=`echo $ImageWidth`
		currentImageWidthHeight="${width}_${height}"
		if [[ ${currentImageWidthHeight} == ${lastHandleImageWidthHeight} ]]; then
			echo ${lastHandleFile} >> ${outpuLogFile}
		fi
	else
		lastHandleFile=${pureImageName}

		imagePath="${imagesFolder}/${imageFullName}"
		ImageWidth=`sips -g pixelWidth ${imagePath} | awk -F: '{print $2}'`
		ImageHeight=`sips -g pixelHeight ${imagePath} | awk -F: '{print $2}'`
		height=`echo $ImageHeight`
		width=`echo $ImageWidth`
		lastHandleImageWidthHeight="${width}_${height}"

		echo "pureImageName = ${pureImageName} lastHandleImageWidthHeight=${lastHandleImageWidthHeight}"
	fi
done

#!/bin/bash
########################
# 脚本功能：从指定目录获取和保存属性到指定的文件
# 输入参数 -i 输入的文件夹
# 输入参数 -o 保存的文件
# 输入参数 -f 使用黑名单和自定义过滤条件的参数
# 输入参数 -c 自定义的黑名单文件
########################

####### 参数定义
param_input_dir=""
param_output_file=""
param_custom_filter_file=""
param_should_use_filter=0

####### 参数解析
while getopts :i:o:c:f opt
do
	case "$opt" in
		i) param_input_dir=$OPTARG
			echo "Found the -i option, with parameter value $OPTARG"
			;;
		o) param_output_file=$OPTARG
			echo "Found the -o option, with parameter value $OPTARG"
			;;
		c) param_custom_filter_file=$OPTARG
			echo "Found the -c option, with parameter value $OPTARG"
			;;
		f) echo "Found the -f option" 
			param_should_use_filter=1
			;;
		*) echo "Unknown option: $opt";;
	esac
done

echo "param_input_dir=${param_input_dir}"
echo "param_output_file=${param_output_file}"
echo "param_custom_filter_file=${param_custom_filter_file}"
echo "param_should_use_filter=${param_should_use_filter}"
# MARK: converted automatically by spec.py. @hgy

Pod::Spec.new do |s|
	s.name = 'IMYTTQ'

	s.version = '7.5.0.018'

	s.description = 'IMYTTQ, 美柚她她圈'
	s.license = 'MIT'
	s.summary = 'IMYTTQ'
	s.homepage = 'http://www.meiyou.com'
	s.authors = { 'haha' => 'xxx@qq.com' }
	s.source = { :git => 'git@gitlab.meiyou.com:iOS/IMYTTQ.git', :branch => 'feature/7.5.0' }
	s.requires_arc = true
	s.ios.deployment_target = '8.0'

	s.source_files = 'IMYTTQ/source/**/*.{h,m}'
	s.resources = 'Resource/**/*.{xib,json,png,jpg,gif,js}','IMYTTQ/source/**/*.{xib,json,png,jpg,gif,js}'
	s.frameworks = 'MediaPlayer'
	
	s.dependency 'IMYAdvertisement'
	s.dependency 'IMY_ViewKit'
	s.dependency 'IMYVideoPlayer'
	s.dependency 'IMYAccount'
	
	s.dependency 'StandardPaths','1.6.6'
	s.dependency 'IMYViewCache','0.4'
	s.dependency 'Myhpple','0.0.3'
	s.dependency 'MXParallaxHeader','0.6.1'
end

#!/bin/bash

# 递归函数读取目录下的所有.m文件
function read_implement_file_recursively {
	echo "read_implement_file_recursively"
	if [[ -d $1 ]]; then
		for item in $(ls $1); do
			itemPath="$1/${item}"
			if [[ -d $itemPath ]]; then
				# 目录
				echo "处理目录 ${itemPath}"
				read_implement_file_recursively $itemPath
				echo "处理目录结束====="
			else 
				# 文件
				echo "处理文件 ${itemPath}"
				newFilePath=`echo ${itemPath} | sed 's/【更多干货daavip朋友圈】//g'`
				mv $itemPath $newFilePath
				echo ""
			fi
		done
	else
		echo "err:不是一个目录"
	fi
}

dir="/Users/aron/学习资料/!小码低层"
read_implement_file_recursively $dir

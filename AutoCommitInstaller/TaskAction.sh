#!/bin/bash

config_file="/Users/aron/softwares/GitAutoCommit/Config/AutoCommitConfig.txt"

for line in $(cat ${config_file} | sed 's/^[ \t]*//g')
do
	if [[ $(expr "$line" : '^#.*') -le 0 ]] ; then
		if [[ -d ${line} ]]; then
			cd ${line}
            echo "Start Action `git config pull.rebase false`"
            # https://juejin.cn/s/git%20config%20pull.rebase%20false%20%23%20%E5%90%88%E5%B9%B6(%E7%BC%BA%E7%9C%81%E7%AD%96%E7%95%A5)
            git config pull.rebase false
            git pull
			git add .
			git commit -m "[udpate] Auto commit"
			git push
		fi
	fi	
done

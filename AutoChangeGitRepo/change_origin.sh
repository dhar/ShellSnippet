#!/bin/sh

containerDir=$1
# read containerDir
# if [[ -d $containerDir ]]; then
# 	echo "图片目录正确"
# else
# 	echo -n "输入的目录无效，请重新执行脚本"
# fi

function read_implement_file_recursively {
	local depth=$2
	local new_depth=$[ ${depth} + 1 ];
	if [[ $depth > 3 ]]; then
		return
	fi
	if [[ -d $1 ]]; then
		for item in $(ls -a $1); do
			if [[ ${item} = "."  ]] || [[ ${item} = ".." ]] ; then
				echo "不处理上级 和 当前"
			else
				itemPath="$1/${item}"
				if [[ -d $itemPath ]]; then
					# 目录
					echo "处理目录 ${itemPath} new_depth=${new_depth}"
					read_implement_file_recursively $itemPath ${new_depth}
					echo "处理目录结束====="
				else 
					# 文件
					echo "处理文件 ${itemPath}"
					if [[ ${item} = "config" ]]; then
						oriurl="http://10.1.0.103:3000/iOS/"
						replacedurl="http://git.babybus.co/frame/ios/"
						# use different delimiters in sed => https://stackoverflow.com/questions/17534840/sed-throws-bad-flag-in-substitute-command
						sed -i '' '{
							s#'"${oriurl}"'#'"${replacedurl}"'#g
						}' ${itemPath}
					fi
				fi
			fi
		done
	else
		echo "err:不是一个目录"
	fi
}

read_implement_file_recursively $containerDir 0
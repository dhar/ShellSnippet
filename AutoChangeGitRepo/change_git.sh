#!/bin/sh

repoName=$1
repoPath=$2

git clone ${repoPath}

dir=${repoName}
oriurl="http://10.1.0.103:3000/iOS/"
replacedurl="http://git.babybus.co/frame/ios/"

cd $dir
podspecfile=`ls *.podspec`
podspecfilePath="./${podspecfile}"
echo "podspecfilePath=${podspecfilePath} oriurl=${oriurl} replacedurl=${replacedurl}"

# use different delimiters in sed => https://stackoverflow.com/questions/17534840/sed-throws-bad-flag-in-substitute-command
sed -i '' '{
	s#'"${oriurl}"'#'"${replacedurl}"'#g
}' ${podspecfilePath}

git add .
git commit -m "update podspec"
git push
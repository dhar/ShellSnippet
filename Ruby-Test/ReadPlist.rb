#!/usr/bin/ruby

plistPath = "/Users/aron/MyWorkSpaces/CodeRepos/bbframework_iOS_New_World/proj.ios/bbframework/Resources/Info.plist"
isChildrenValue = `/usr/libexec/PlistBuddy -c "Print Children" #{plistPath}`
isChildrenValue = "#{isChildrenValue}".rstrip()
puts "isChildrenValue =#{isChildrenValue}="

puts "=============(casecmp)"

if (isChildrenValue.casecmp("false")) then
	puts "1111-非儿童版"
else
	puts "1111-儿童版"
end


puts "=============(==)"

if (isChildrenValue == "false") then
	puts "3333-非儿童版"
else
	puts "3333-儿童版"
end


puts "=============(===)"
if (isChildrenValue === "false") then
	puts "3333-非儿童版"
else
	puts "3333-儿童版"
end


puts "=============(equal)"
if (isChildrenValue.equal?("false")) then
	puts "3333-非儿童版"
else
	puts "3333-儿童版"
end

puts "=============(eql?)"
if (isChildrenValue.eql?("false")) then
	puts "3333-非儿童版"
else
	puts "3333-儿童版"
end
#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import os

def upload_dsym_to_bugly():
    """
    上传DSYM符号表到Bugly
    :return:
    """
    # bugly_upload_jar_path = os.path.join(BASH_PATH, "buglySymboliOS.jar")
    bugly_upload_jar_path = "/Users/aron/Downloads/buglySymboliOS3.0.0/buglySymboliOS.jar"
    # dsym_sub_path = "bin/%s.xcarchive/dSYMs/%s.app.dSYM" % (self.get_target(), self.get_target())
    # dysm_file_path = os.path.join(self.template_proj_dir, dsym_sub_path) # "/Volumes/data/BuildCache/19_11_23_pack/proj.ios/bin/2d_noSuper_education.xcarchive/dSYMs/2d_noSuper_education.app.dSYM"
    dysm_file_path = "/Volumes/data/BuildCache/19_11_23_pack/proj.ios/bin/2d_noSuper_education.xcarchive/dSYMs/2d_noSuper_education.app.dSYM";
    id = "6b76377f04"
    key = "666182f1-c3f4-465c-a9d7-f686c075fdb3"
    # package = self.app_id #"com.sinyee.babybus.ranch"
    package = "com.sinyee.babybus.ranch"
    # version = self.get_result_version() #"10.01.1500"
    version = "10.01.1500"
    upload_bugly_cmd = "java -jar " + bugly_upload_jar_path + "" \
          " -i " + dysm_file_path + ""\
          " -u -id " + id + "" \
          " -key " + key + "" \
          " -package" + package + "" \
          " -version " + version + ""
    os.system(upload_bugly_cmd)

upload_dsym_to_bugly()
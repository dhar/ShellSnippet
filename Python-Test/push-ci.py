#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import os

ci_dir = "/Users/aron/MyWorkSpaces/CodeRepos/bbframework_iOS_New-CI"
source_dir = "/Users/aron/MyWorkSpaces/CodeRepos/bbframework_iOS_New_World"

# ci_dir = ""
# source_dir = ""
branch = "master_world"

# os.system('cd %s' % ci_dir)
os.chdir(ci_dir)
os.system('git checkout %s' % branch)
os.system('rm -rf *')
os.system('cp -r %s/proj.ios %s/res %s/src %s' % (source_dir, source_dir, source_dir, ci_dir))

message = "[ci] 世界 10.01.1200"
os.system('git add .')
os.system('git commit -m \"%s\"' % message)
os.system('git push --set-upstream origin')
#!/bin/bash

SHELL_CMD=/Users/aron/Downloads/libwebp-1.1.0-mac-10.15/bin/cwebp
source_image_dir="/Users/aron/Downloads/homeImages"
dest_image_dir="/Users/aron/Downloads/homeImages-webp"

# 递归函数读取目录下的所有.m文件
function read_implement_file_recursively {
	if [[ -d $1 ]]; then
		fullDir="$1/$2"
		for item in $(ls ${fullDir}); do
			subPath="$2/${item}";
			fileItemPath="$1/${subPath}"
			if [[ -d $fileItemPath ]]; then

				# 新建临时目录
				destDir="${dest_image_dir}/${subPath}"
				mkdir -p ${destDir}

				# 目录
				echo "处理目录 ${fileItemPath} => ${destDir}"
				read_implement_file_recursively $1 $subPath

				echo "处理目录结束====="
			else 
				# 文件
				newFileSubPath=`echo ${subPath} | sed 's/png/webp/g'`
				newFilePath="${dest_image_dir}/${newFileSubPath}"
				echo "处理文件 ${fileItemPath} => ${newFilePath}"
				# mv $fileItemPath $newFilePath
				$SHELL_CMD ${fileItemPath} -o ${newFilePath}
				echo ""
			fi
		done
	else
		echo "err:不是一个目录"
	fi
}

read_implement_file_recursively $source_image_dir ""

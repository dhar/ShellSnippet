# 使用说明：
# restoreSymbol.sh (dsym文件路径) (崩溃的堆栈日志路径) (需要解析的Mach-O，比如Seeyou) (基地址) (arm架构[arm64,armv7 默认为arm64])
# Example: ./restoreSymbol.sh  2d_noSuper_education.app.dSYM un_symbocated.txt 2d_noSuper_education 0x0000000102de0000
# 输出结果：对应的行数 堆栈信息

dsym_dir=$1
crash_log_dir=$2
app=$3
slide_address=$4
armtype=arm64
real_dsym=""

# 获取DWARF文件
DWARF="${dsym_dir}/Contents/Resources/DWARF/*"
for file in ${DWARF}
do
real_dsym=$file
done

# 获取armtype
if [[ -n $5 ]]; then
	armtype=$5
fi

cat ${crash_log_dir} | while read line
do
	arr=($line)
	length=${#arr[@]}
	if [[ $length > 2 ]]; then
		if [[ ${arr[1]} == ${app}  ]]; then
			echo "${arr[0]} \c"
			# 解析堆栈数据
			address=${arr[2]}
			xcrun atos -arch ${armtype} -o ${real_dsym} -l ${slide_address} ${address}
		else
			# 直接打印原始堆栈数据
			echo $line
		fi
	fi
done
